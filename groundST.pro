TARGET = groundST
TEMPLATE = app
QT += opengl
SOURCES += main.cpp \
	mainwindow.cpp \
	portconfig.cpp \
	port.cpp \
	terminalstyle.cpp \
	terminalhistory.cpp \
	terminal.cpp \
	terminalsettings.cpp \
	terminalcommands.cpp \
    sensors.cpp
HEADERS += mainwindow.h \
	portconfig.h \
	port.h \
	terminalstyle.h \
	terminalhistory.h \
	terminal.h \
	terminalsettings.h \
	terminalcommands.h \
    sensors.h \
    XL345.h
FORMS += mainwindow.ui \
	about.ui \
	portconfig.ui \
	terminalstyle.ui \
	terminalsettings.ui \
    sensors.ui
LIBS += -L../qextserialport/lib \
		-lqextserialport-1.2
INCLUDEPATH += ../qextserialport/src
unix:DEFINES += _TTY_POSIX_
win32:DEFINES += _TTY_WIN_
CONFIG += debug
RESOURCES += groundST.qrc
