#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "terminalstyle.h"
#include "portconfig.h"
#include "sensors.h"
#include <QSettings>
#include <QMessageBox>
#include <QFileDialog>
#include <QTimer>
#include <QAction>
#include <QDateTime>
#include <QPalette>
#include "ui_about.h"
#include <QScopedPointer>

#include "qextserialenumerator.h"

MainWindow::MainWindow (QWidget *parent) : QMainWindow (parent), ui (new Ui::MainWindow), uart(0)
{
	ui->setupUi (this);

	// Create the terminal widget
	term = new Terminal ();
	setCentralWidget (term);

	term->installEventFilter (this);

	// Create UART object
	uart = new Port ();

	createActions ();
	addToolBarButtons ();
	createStatusBarLabels ();

	loadStyleConfiguration ();
	clearSerialInfo ();
}
MainWindow::~MainWindow()
{
	if (uart)
	{
		delete uart;
		uart = NULL;
	}
}

void MainWindow::addToolBarButtons ()
{
	ui->toolBar->addAction (conDiscAct);
	ui->toolBar->addAction (portConfigAct);
	ui->toolBar->addAction (configureColorsAct);
	ui->toolBar->addAction (configureTerminalAct);
	ui->toolBar->addSeparator ();
	ui->toolBar->addAction (sensorsAct);
	ui->toolBar->addSeparator ();
	ui->toolBar->addAction (quitAct);
}

void MainWindow::createActions ()
{
	conDiscAct = new QAction (QIcon (":/img/images/disconnect.png"), tr ("Connect"), this);
	conDiscAct->setShortcut (tr ("F10"));
	conDiscAct->setStatusTip (tr ("Connect"));
	connect (conDiscAct, SIGNAL (triggered ()), this, SLOT (connectDisconnect()));

	portConfigAct = new QAction (QIcon (":/img/images/configure.png"), tr ("Configure Serial Port"), this);
	portConfigAct->setShortcut (tr ("Ctrl+1"));
	portConfigAct->setStatusTip (tr ("Configure Serial Port"));
	connect (portConfigAct, SIGNAL (triggered ()), this, SLOT (configurePort ()));

	configureColorsAct = new QAction (QIcon (":/img/images/palette.png"), tr ("Terminal Style"), this);
	configureColorsAct->setShortcut (tr ("Ctrl+2"));
	configureColorsAct->setStatusTip (tr ("Terminal Style"));
	connect (configureColorsAct, SIGNAL (triggered ()), this, SLOT (configureColors ()));

	configureTerminalAct = new QAction (QIcon (":/img/images/preferences.png"), tr ("Terminal Settings"), this);
	configureTerminalAct->setShortcut (tr ("Ctrl+3"));
	configureTerminalAct->setStatusTip (tr ("Terminal Settings"));
	connect (configureTerminalAct, SIGNAL (triggered ()), this, SLOT (configureTerminal ()));

	sensorsAct = new QAction (QIcon (":/img/images/sensors.png"), tr ("Sensors"), this);
	sensorsAct->setShortcut (tr ("Ctrl+4"));
	sensorsAct->setStatusTip (tr ("Sensors"));
	connect (sensorsAct, SIGNAL (triggered ()), this, SLOT (sensorsDialog ()));

	quitAct = new QAction (QIcon (":/img/images/exit.png"), tr ("Exit groundST"), this);
	quitAct->setShortcut (tr ("Alt+F4"));
	quitAct->setStatusTip (tr ("Exit groundST"));
	connect (quitAct, SIGNAL (triggered ()), this, SLOT (quit ()));

	connect (ui->aboutAct, SIGNAL (triggered ()), this, SLOT (helpAbout ()));
	connect (ui->aboutQtAct, SIGNAL (triggered ()), this, SLOT (aboutQt ()));

	connect (ui->saveScreenAct, SIGNAL (triggered ()), this, SLOT (saveScreen ()));
	connect (ui->beginLogAct, SIGNAL (triggered ()), this, SLOT (startLogging ()));
	connect (ui->endLogAct, SIGNAL (triggered ()), this, SLOT (endLogging ()));

	connect (ui->selectAllAct, SIGNAL (triggered ()), this, SLOT (selectAll ()));
	connect (ui->copyAct, SIGNAL (triggered ()), this, SLOT (copy ()));
	connect (ui->pasteAct, SIGNAL (triggered ()), this, SLOT (paste ()));
	connect (ui->clearScreenAct, SIGNAL (triggered ()), this, SLOT (clearScreen ()));
}

void MainWindow::poll ()
{
	_labelRx->setPixmap (QPixmap (":/img/images/ledgreen.png"));
	_labelRx->setPixmap (QPixmap (":/img/images/ledred.png"));
	int resp = uart->pollSerial();
	if (resp == 0 && !uart->receivedBuffer.isEmpty ())
	{
		term->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
		QString buf = uart->receivedBuffer;
		bool r = uart->receivedBuffer.endsWith ('\r');
		bool n = uart->receivedBuffer.endsWith ('\n');
		bool rn = uart->receivedBuffer.endsWith ("\r\n") | buf.endsWith ("\n\r");
		if (r && !n && !rn) //Treat here end of line
		{
			term->moveCursor (QTextCursor::StartOfLine, QTextCursor::KeepAnchor);
			term->textCursor ().removeSelectedText ();
			term->addTimeStampCR ();
			term->insertPlainText (uart->receivedBuffer.simplified ());
		}
		else if (!r && (n || rn))
		{
			term->insertPlainText (uart->receivedBuffer.simplified ());
			term->addTimeStamp();
		}
		else
		{
			term->insertPlainText (uart->receivedBuffer);
			term->addTimeStamp();
		}
		term->ensureCursorVisible();
	}
	// Change led color
	_labelRx->setPixmap (QPixmap (":/img/images/ledred.png"));
}

void MainWindow::connectDisconnect()
{
	int resp = uart->connectDisconnect();
	if (resp == 0)
	{
		conDiscAct->setIcon (QIcon (":/img/images/connect.png"));
		timer = new QTimer (this);
		connect (timer, SIGNAL (timeout()), this, SLOT (poll()));
		conDiscAct->setStatusTip (tr ("Disconnect"));
		conDiscAct->setToolTip ( tr ("Disconnect"));
		conDiscAct->setText ( tr ("Disconnect"));
		timer->start(100);
		populateSerialInfo ();
	}
	else
	{
		conDiscAct->setIcon (QIcon (":/img/images/disconnect.png"));
		conDiscAct->setStatusTip (tr ("Connect"));
		conDiscAct->setToolTip( tr ("Connect"));
		conDiscAct->setText (tr ("Connect"));
		if (timer->isActive())
		{
			timer->stop ();
			delete timer;
			timer = NULL;
		}
		clearSerialInfo ();
		_labelRx->setPixmap (QPixmap (":/img/images/ledyellow.png"));
		_labelTx->setPixmap (QPixmap (":/img/images/ledyellow.png"));
	}
}

void MainWindow::configureColors ()
{
	TerminalStyle *conf = new TerminalStyle (this, term);
	conf->exec ();
	if (conf)
	{
		delete conf;
		conf = NULL;
	}
}

void MainWindow::configurePort ()
{
	QScopedPointer <PortConfig> portConf (new PortConfig (this, uart));
	portConf->exec ();
}

void MainWindow::configureTerminal ()
{
	QScopedPointer <TerminalSettings> ts (new TerminalSettings (this, term));
	ts->exec ();
}

void MainWindow::sensorsDialog ()
{
	if (uart->isOpen ())
	{
		QScopedPointer <Sensors> sd (new Sensors (this, uart));
		sd->exec ();
	}
	else
	{
		term->updateTypedCmd (tr ("UART port isn't open, please configure the serial port before."));
		term->addTimeStamp();
		term->ensureCursorVisible();
	}
}

void MainWindow::quit()
{
	QCoreApplication::quit();
}

// Grab keypresses meant for edit, send to serial port.
bool MainWindow::eventFilter (QObject *obj, QEvent *event)
{
	if (event->type() == QEvent::KeyPress)
	{
		QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
		QString s = keyEvent->text();
		// get Enter keys from normal keyboard and keypad
		if (keyEvent->key() == Qt::Key_Return || keyEvent->key () == Qt::Key_Enter)
		{
			term->addToHistory ();
			QString typedCmd = term->getTypedCmd();
			if (!term->parseCommand (typedCmd))
			{
				typedCmd.push_back ("\r");
				if (uart->isOpen ())
				{
					_labelTx->setPixmap (QPixmap (":/img/images/ledgreen.png"));
					_labelRx->setPixmap (QPixmap (":/img/images/ledred.png"));
					if (term->treatBeforeSend (typedCmd))
						uart->sendBytes (typedCmd);
				}
			}
			term->addTimeStamp();
			term->ensureCursorVisible();
			return true;
		}
		else if (keyEvent->key () == Qt::Key_Backspace)
		{
			term->backspace ();
			return true;
		}
		else if (keyEvent->key () == Qt::Key_Tab)
		{
			return true;
		}
		else if (keyEvent->key() == Qt::Key_Up)
		{
			term->nextOnCmdHistory ();
			term->updateCmdHistorydOnScreen();
			return true;
		}
		else if (keyEvent->key() == Qt::Key_Down)
		{
			term->previousOnCmdHistory ();
			term->updateCmdHistorydOnScreen();
			return true;
		}
		// Ctrl + C
		else if (keyEvent->key() == Qt::Key_C && keyEvent->modifiers ().testFlag (Qt::ControlModifier))
		{
			term->copy ();
			return true;
		}
		else if (keyEvent->key() == Qt::Key_X && keyEvent->modifiers ().testFlag (Qt::ControlModifier))
		{
			// Don't do it
			return true;
		}
		else if (keyEvent->key() == Qt::Key_V && keyEvent->modifiers ().testFlag (Qt::ControlModifier))
		{
			term->paste ();
			return true;
		}
		else if (keyEvent->key() == Qt::Key_A && keyEvent->modifiers ().testFlag (Qt::ControlModifier))
		{
			term->selectAll ();
			return true;
		}
		// This teste must be after every other combination of Ctrl+..., otherwise it won't work
		else if (keyEvent->modifiers ().testFlag (Qt::ControlModifier))
		{
			//Don't do anything
			return true;
		}
		else
		{
			term->updateTypedCmd (s);
			return true;
		}
	}
	else
		// standard event processing
		return QObject::eventFilter (obj, event);
}

void MainWindow::helpAbout ()
{
	QDialog dlg;
	Ui_aboutDialog dlg_ui;
	dlg_ui.setupUi(&dlg);

	dlg.exec();
}

void MainWindow::saveScreen ()
{
	term->saveScreen ();
}

void MainWindow::startLogging ()
{
	term->startLogging ();
}

void MainWindow::endLogging ()
{
	term->endLogging ();
}

void MainWindow::clearScreen ()
{
	term->clearScreen ();
	term->addTimeStamp();
}

void MainWindow::copy()
{
	term->copy ();
}

void MainWindow::paste ()
{
	term->paste ();
}

void MainWindow::selectAll ()
{
	term->selectAll ();
}

void MainWindow::aboutQt ()
{
	QMessageBox::aboutQt (this, "About Qt");
}

void MainWindow::loadStyleConfiguration ()
{
	QPalette pal = term->palette ();
	QFont font ("Courier New", 10);
	QColor backColor;
	QColor fontColor;
	QSettings settings ("groundST", "style");

	/// If exists at least one setting, testing for background color
	if (settings.contains ("backColor"))
	{
		backColor = settings.value ("backColor").value<QColor>();
		fontColor = settings.value ("fontColor").value<QColor>();
		font = settings.value ("font").value<QFont>();
	}
	else
	{
		backColor = Qt::black;
		fontColor = Qt::white;
	}
	pal.setColor (QPalette::Base, backColor);
	pal.setColor (QPalette::Text, fontColor);

	term->setPalette (pal);
	term->setAutoFillBackground (true);
	term->setFont (font);
}

void MainWindow::createStatusBarLabels ()
{
	_labelTx = new QLabel (this);
	_labelTx->setMinimumSize (QSize (15, 15));
	_labelTx->setMaximumSize (QSize (15, 15));
	_labelTx->setPixmap (QPixmap (QString::fromUtf8 (":/img/images/ledyellow.png")));
	_labelTx->setScaledContents (true);
	QSizePolicy sizePolicyTx (QSizePolicy::Fixed, QSizePolicy::Fixed);
	sizePolicyTx.setHorizontalStretch (0);
	sizePolicyTx.setVerticalStretch (0);
	sizePolicyTx.setHeightForWidth (_labelTx->sizePolicy().hasHeightForWidth ());
	_labelTx->setSizePolicy (sizePolicyTx);

	_labelRx = new QLabel (this);
	_labelRx->setMinimumSize (QSize (15, 15));
	_labelRx->setMaximumSize (QSize (15, 15));
	_labelRx->setPixmap (QPixmap (QString::fromUtf8 (":/img/images/ledyellow.png")));
	_labelRx->setScaledContents (true);
	QSizePolicy sizePolicyRx (QSizePolicy::Fixed, QSizePolicy::Fixed);
	sizePolicyRx.setHorizontalStretch (0);
	sizePolicyRx.setVerticalStretch (0);
	sizePolicyRx.setHeightForWidth (_labelRx->sizePolicy().hasHeightForWidth ());
	_labelRx->setSizePolicy (sizePolicyRx);

	statusBarWidgetList.append (_labelTx);
	statusBarWidgetList.append (_labelRx);

	statusBarWidgetList.at (0)->hide();
	statusBarWidgetList.at (1)->hide();

	int loop;
	for (loop = 0; loop < 6; loop++)
	{
		// Put a label in each box
		QLabel *label = new QLabel ();
		label->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
		statusBarWidgetList.append (label);
	}
}

void MainWindow::populateSerialInfo ()
{
	QStringList tmp;

	int loop;
	int len = statusBarWidgetList.size ();
	for (loop = 0; loop < len; loop++)
	{
		statusBarWidgetList.at (loop)->hide();
		ui->statusBar->removeWidget (statusBarWidgetList.at (loop));
	}

	for (loop = 0; loop < len; loop++)
	{
		ui->statusBar->addWidget (statusBarWidgetList.at (loop));
		statusBarWidgetList.at (loop)->show();
	}

	QLabel *label;
	label = static_cast<QLabel*> (statusBarWidgetList.at (2));
	tmp = uart->getPorts ();
	label->setText (tr (" :: Port: ") + tmp.at (uart->portS.port));
	label->setFixedWidth (label->minimumSizeHint ().width ());

	label = static_cast<QLabel*> (statusBarWidgetList.at (3));
	tmp = uart->getBaud ();
	label->setText (tr (" :: Baud: ") + tmp.at (uart->portS.baud));
	label->setFixedWidth (label->minimumSizeHint ().width ());

	label = static_cast<QLabel*> (statusBarWidgetList.at (4));
	tmp = uart->getDataBits ();
	label->setText (tr (" :: Data: ") + tmp.at (uart->portS.dataBits));
	label->setFixedWidth (label->minimumSizeHint ().width ());

	label = static_cast<QLabel*> (statusBarWidgetList.at (5));
	tmp = uart->getParity ();
	label->setText (tr (" :: Parity: ") + tmp.at (uart->portS.parity));
	label->setFixedWidth (label->minimumSizeHint ().width ());

	label = static_cast<QLabel*> (statusBarWidgetList.at (6));
	tmp = uart->getStopBits ();
	label->setText (tr (" :: Stop: ") + tmp.at (uart->portS.stopBits));
	label->setFixedWidth (label->minimumSizeHint ().width ());

	label = static_cast<QLabel*> (statusBarWidgetList.at (7));
	tmp = uart->getFlow ();
	label->setText (tr (" :: Flow: ") + tmp.at (uart->portS.flow));
	label->setFixedWidth (label->minimumSizeHint ().width ());
}

void MainWindow::clearSerialInfo ()
{
	int loop;
	int len = statusBarWidgetList.size ();
	for (loop = 0; loop < len; loop++)
	{
		statusBarWidgetList.at (loop)->hide();
		ui->statusBar->removeWidget (statusBarWidgetList.at (loop));
	}
}
