#ifndef SENSORS_H
#define SENSORS_H

#include <QDialog>
#include "port.h"

class Port;

namespace Ui
{
	class Sensors;
}

class Sensors : public QDialog
{
		Q_OBJECT

	public:
		explicit Sensors (QWidget *parent = 0, Port *p = 0);
		~Sensors ();

	private slots:
		void on_pushButtonDevId_clicked();

	private:
		Ui::Sensors *ui;
		Port *uart;
};

#endif // SENSORS_H
