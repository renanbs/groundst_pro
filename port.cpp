#include "port.h"

Port::Port(QObject *parent) : QObject(parent), uartPort (0)
{
	setupBaud();
	setupParity();
	setupFlowControl();
	setupDataBits();
	setupStopBits();
	listAvailablePorts();
	readPortConfig (&portS);
	receivedBuffer.clear();	// Clear buffer from serial
}

Port::~Port ()
{

}

void Port::setupBaud ()
{
	intBaud[0] = BAUD300;
	intBaud[1] = BAUD2400;
	intBaud[2] = BAUD4800;
	intBaud[3] = BAUD9600;
	intBaud[4] = BAUD19200;
	intBaud[5] = BAUD38400;
	intBaud[6] = BAUD57600;
	intBaud[7] = BAUD115200;
	stringListBaud.append (tr ("300"));
	stringListBaud.append (tr ("2400"));
	stringListBaud.append (tr ("4800"));
	stringListBaud.append (tr ("9600"));
	stringListBaud.append (tr ("19200"));
	stringListBaud.append (tr ("38400"));
	stringListBaud.append (tr ("57600"));
	stringListBaud.append (tr ("115200"));
}

void Port::setupParity ()
{
	intParity[0] = PAR_NONE;
	intParity[1] = PAR_ODD;
	intParity[2] = PAR_EVEN;
	intParity[3] = PAR_SPACE;
	stringListParity.append (tr ("None"));
	stringListParity.append (tr ("Odd"));
	stringListParity.append (tr ("Even"));
	stringListParity.append (tr ("Space"));
#ifdef _TTY_WIN_
	intParity[4] = PAR_MARK;
	stringListParity.append (tr ("Mark"));
#endif
}

void Port::setupFlowControl ()
{
	intFlow[0] = FLOW_OFF;
	intFlow[1] = FLOW_HARDWARE;
	intFlow[2] = FLOW_XONXOFF;
	stringListFlowControl.append (tr ("None"));
	stringListFlowControl.append (tr ("Hardware"));
	stringListFlowControl.append (tr ("Xon/Xoff"));
}

void Port::setupDataBits ()
{
	intDataBits[0] = DATA_5;
	intDataBits[1] = DATA_6;
	intDataBits[2] = DATA_7;
	intDataBits[3] = DATA_8;
	stringListDataBits.append (tr ("5"));
	stringListDataBits.append (tr ("6"));
	stringListDataBits.append (tr ("7"));
	stringListDataBits.append (tr ("8"));
}

void Port::setupStopBits ()
{
	intStopBits[0] = STOP_1;
	intStopBits[1] = STOP_2;
	stringListStopBits.append (tr ("1"));
	stringListStopBits.append (tr ("2"));
#ifdef _TTY_WIN_
	stringListStopBits.append (tr ("1.5"));
	intStopBits[2] = STOP_1_5;
#endif
}

void Port::listAvailablePorts ()
{
	// Find serial ports available
	QStringList devices;
	int i;
#ifdef _TTY_POSIX_
	QDir dir ("/dev");
	QStringList filterList ("ttyS*");
	filterList += QStringList ("ttyUSB*");
	filterList += QStringList ("ttyACM*");
	dir.setNameFilters (filterList);
	devices = dir.entryList (QDir::System);
#elif defined(_TTY_WIN_)
	int j;
	QList <QextPortInfo> list = QextSerialEnumerator::getPorts ();

	for (i = 0; i < list.count (); i++)
	{
		QString s = list.at (i).portName;
		char t[20];
		for (j = 0; j < s.length (); j++)
		{
			if (!isprint ((int)s.at (j).toAscii ()))
			{
				s.resize (j);
				break;
			}
			else
				t[j] = s.at (j).toAscii ();
		}
		devices.append (s);
	}
#endif

	for (i = 0; i < devices.count (); i++)
	{
		if (!devices.at (i).contains ("ttys"))
			stringListPorts.append (devices.at (i));
	}
}

QStringList Port::getBaud ()
{
	return stringListBaud;
}

QStringList Port::getParity ()
{
	return stringListParity;
}

QStringList Port::getFlow ()
{
	return stringListFlowControl;
}

QStringList Port::getDataBits ()
{
	return stringListDataBits;
}

QStringList Port::getStopBits ()
{
	return stringListStopBits;
}

QStringList Port::getPorts ()
{
	return stringListPorts;
}

void Port::readPortConfig (po *p)
{
	QSettings settings ("groundST", "config");
	p->port  = settings.value ("port").toInt();
	p->baud = settings.value ("baud").toInt();
	p->parity = settings.value ("parity").toInt();
	p->flow = settings.value ("flow").toInt();
	p->dataBits = settings.value ("databits").toInt();
	p->stopBits = settings.value ("stopbits").toInt();
}

void Port::savePortConfig (po *p)
{
	QSettings settings ("groundST", "config");
	settings.setValue ("port", p->port);
	settings.setValue ("baud", p->baud);
	settings.setValue ("parity", p->parity);
	settings.setValue ("flow", p->flow);
	settings.setValue ("databits", p->dataBits);
	settings.setValue ("stopbits", p->stopBits);
}

// ret 0 - Connection Done
// ret 1 - Disconnection Done
// ret 2 - NOK
int Port::connectDisconnect()
{
	int ret = 2;
	if (uartPort)
	{
		// close it
		delete uartPort;
		uartPort = NULL;
		ret = 1;
	}
	else
	{
		//listAvailablePorts ();
		QString device;
#ifdef _TTY_POSIX_
		device = QString ("/dev/");
#else
		device.clear ();
#endif
		device += stringListPorts.at (portS.port);
		uartPort = new QextSerialPort(device, QextSerialPort::Polling);
		uartPort->setBaudRate ((BaudRateType)intBaud[portS.baud]);
		uartPort->setDataBits ((DataBitsType)intDataBits[portS.dataBits]);
		uartPort->setParity ((ParityType)intParity[portS.parity]);
		uartPort->setStopBits ((StopBitsType)intStopBits[portS.stopBits]);
		uartPort->setFlowControl ((FlowType)intFlow[portS.flow]);
		uartPort->setTimeout (100);

		if (!uartPort->open(QIODevice::ReadWrite))
		{
			delete uartPort;
			uartPort = NULL;
			ret = 2;
		}
		else
		{
			ret = 0;
#ifdef _TTY_WIN_
			// For win32, you have to re-do this crap after opening.
			uartPort->setBaudRate((BaudRateType)intBaud[portS.baud]);
			uartPort->setDataBits((DataBitsType)intDataBits[portS.dataBits]);
			uartPort->setParity((ParityType)intParity[portS.parity]);
			uartPort->setStopBits((StopBitsType)intStopBits[portS.stopBits]);
			uartPort->setFlowControl((FlowType)intFlow[portS.flow]);
			uartPort->setTimeout(0);
#endif
		}
	}
	return ret;
}

void Port::sendBytes (QString b)
{
	if (uartPort->isOpen ())
		uartPort->write (b.toLatin1());
}

int Port::pollSerial ()
{
	// Check for serial chars
	if (!uartPort)
		return 1;

	QByteArray bytes = uartPort->readAll();

	if (bytes.isEmpty())
		return 1;

		receivedBuffer = bytes;
		return 0;
}

bool Port::isOpen ()
{
	if (uartPort)
		return true;
	else
		return false;
}
