#include "sensors.h"
#include "ui_sensors.h"

Sensors::Sensors (QWidget *parent, Port *p) : QDialog (parent), ui (new Ui::Sensors)
{
	ui->setupUi (this);
	uart = p;
}

Sensors::~Sensors ()
{
	delete ui;
}

void Sensors::on_pushButtonDevId_clicked()
{
	uart->sendBytes ("0");
	ui->lineEditDevId->setText (uart->receivedBuffer);
}
