#ifndef TERMINALCOMMANDS_H
#define TERMINALCOMMANDS_H

#include <QObject>
#include <QCoreApplication>
#include "terminal.h"

class Terminal;
class TerminalCommands;

struct commands
{
	const char* name;
	void (TerminalCommands::*funct)();
};

class TerminalCommands : public QObject
{
		Q_OBJECT
	public:
		explicit TerminalCommands (QObject *parent = 0, Terminal *t = 0);
		virtual ~TerminalCommands ();
		Terminal *term;
		void help ();
		void clear ();
		void version ();
		void exit ();
		void history ();
		bool parseCommand (const QString &str);

		static commands cmds[];

	signals:

	public slots:

	private:

};

#endif // TERMINALCOMMANDS_H
