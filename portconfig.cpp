#include "portconfig.h"
#include "ui_portconfig.h"

PortConfig::PortConfig(QWidget *parent, Port *p) : QDialog(parent), ui(new Ui::PortConfig)
{
	ui->setupUi(this);
	uart = p;

	ui->comboBoxBaudRate->addItems (uart->getBaud());
	ui->comboBoxParity->addItems (uart->getParity());
	ui->comboBoxFlowControl->addItems (uart->getFlow());
	ui->comboBoxDataBits->addItems (uart->getDataBits());
	ui->comboBoxStopBits->addItems (uart->getStopBits());
	ui->comboBoxPort->addItems (uart->getPorts());

	loadPortConfig();
}

PortConfig::~PortConfig()
{
	delete ui;
}

int PortConfig::savePortConfig ()
{
	uart->portS.port = ui->comboBoxPort->currentIndex();
	uart->portS.baud = ui->comboBoxBaudRate->currentIndex();
	uart->portS.parity = ui->comboBoxParity->currentIndex();
	uart->portS.flow = ui->comboBoxFlowControl->currentIndex();
	uart->portS.dataBits = ui->comboBoxDataBits->currentIndex();
	uart->portS.stopBits = ui->comboBoxStopBits->currentIndex();
	return true;
}

int PortConfig::loadPortConfig ()
{
	// If there are no settings (first run), all values are zero
	ui->comboBoxPort->setCurrentIndex (uart->portS.port);
	ui->comboBoxBaudRate->setCurrentIndex (uart->portS.baud);
	ui->comboBoxParity->setCurrentIndex (uart->portS.parity);
	ui->comboBoxFlowControl->setCurrentIndex (uart->portS.flow);
	ui->comboBoxDataBits->setCurrentIndex (uart->portS.dataBits);
	ui->comboBoxStopBits->setCurrentIndex (uart->portS.stopBits);
	return true;
}

void PortConfig::on_buttonBox_accepted()
{
	savePortConfig();
	uart->savePortConfig(&uart->portS);
}
